package com.simplemobiletools.tscanner.fileprovider.extensions

fun String.isZipFile() = endsWith(".zip", true)
