package com.simplemobiletools.tscanner.scanner;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import com.simplemobiletools.tscanner.R;
import com.simplemobiletools.tscanner.scanner.helpers.MyConstants;
import static com.simplemobiletools.tscanner.scanner.helpers.MyConstants.CAMERA_REQUEST;

public class UploadImageActivity extends AppCompatActivity {

    Button btnOpenGallery;
    Button btnOpenCamera;

    Uri selectedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);
        initializeElement();
        initializeEvent();
    }

    private void initializeElement() {
        this.btnOpenGallery = findViewById(R.id.btnOpenGallery);
        this.btnOpenCamera = findViewById(R.id.btnOpenCamera);
    }

    private void initializeEvent() {
        this.btnOpenGallery.setOnClickListener(this.btnOpenGalleryClick);
        this.btnOpenCamera.setOnClickListener(this.btnOpenCameraClick);
    }

    private View.OnClickListener btnOpenGalleryClick = v -> {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, MyConstants.GALLERY_IMAGE_LOADED);
    };

    private View.OnClickListener btnOpenCameraClick = v -> {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                selectedImage = getImageUri(bitmap);
                loadImage();
                break;
            case MyConstants.GALLERY_IMAGE_LOADED:
                if (resultCode == RESULT_OK && data != null) {
                    selectedImage = data.getData();
                    loadImage();
                }
                break;
        }
    }

    private void loadImage() {
        Intent intent = new Intent(UploadImageActivity.this, ImageCropActivity.class);
        intent.putExtra("uri", selectedImage);
        startActivity(intent);
    }

    public Uri getImageUri(Bitmap bitmap) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

}
